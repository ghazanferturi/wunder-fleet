<?php

namespace App\Repository;

use App\Entity\RegistrationDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RegistrationDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistrationDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistrationDetails[]    findAll()
 * @method RegistrationDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationDetails::class);
    }

    // /**
    //  * @return RegistrationDetails[] Returns an array of RegistrationDetails objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegistrationDetails
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
