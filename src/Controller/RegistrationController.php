<?php

namespace App\Controller;

use App\Entity\RegistrationDetails;
use App\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="registration")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $iban = $this->get('session')->get('iban');

        //$iban = 'DE1234567891012';

        $em = $this->getDoctrine()->getManager();
        $customer = $em->getRepository(RegistrationDetails::class)->findOneBy(['iban' => $iban]);

        $paymentId = 'Something Went Wrong!';

        if ($customer instanceof RegistrationDetails) {
            try {
                $demo_payment_response = $this->restApiClient($customer->getId(), $iban, $customer->getAccountOwner());
                $paymentId = $demo_payment_response['paymentDataId'];
            } catch (ClientExceptionInterface $e) {
            } catch (DecodingExceptionInterface $e) {
            } catch (RedirectionExceptionInterface $e) {
            } catch (ServerExceptionInterface $e) {
            } catch (TransportExceptionInterface $e) {
            }

            $customer->setPaymentDataId($paymentId);
            $em->flush();

        } else {
            throw $this->createNotFoundException(
                'No customer found'
            );
        }

        return $this->render('registration/index.html.twig', [
            'controller_name' => 'RegistrationController',
            'paymentId' => $paymentId
        ]);
    }

    /**
     * @Route("/add", name="add_new")
     * @param Request $request
     * @return Response
     */
    public function register(Request $request): Response
    {
        $registrationInfo = new RegistrationDetails();

        $form = $this->createForm(RegistrationType::class, $registrationInfo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $iban = $data->getIban();

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            $this->get('session')->set('iban', $iban);

            return $this->redirectToRoute('registration');
        }

        return $this->render('registration/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param int $customerId
     * @param string $iban
     * @param string $owner
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @return array
     */
    public function restApiClient(int $customerId, string $iban, string $owner): array
    {
        $client = HttpClient::create();

        $response = $client->request('POST',
            'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'json' => [
                    'customerId' => $customerId,
                    'iban' => $iban,
                    'owner' => $owner
                ]
            ]);

        return $response->toArray();
    }

}
