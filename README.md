# WunderFleet - v1.0.0
WunderFleet Registration using Symfony (PHP)

## Author
- Ghazanfar Abbas

## Technology Stack
- PHP
  - Symfony 4
  - Doctrine
  - Twig Template Engine
  - Bootstrap 4.0

## Requirements
- PHP 7.1
  - PHP MySQL driver
- MySQL
- Composer

## Setup
- Clone project & Change to project directory
- Navigate to project directory and give write access to project directory:
    - Run `$ cd wuunder-fleet` navigate to project directory
    - Run `$ sudo chmod -R 777 ./` give write access so that composer install files.
- Run `composer install` insider project directory to install required symfony, doctrine & web-server dependencies.
- Create MySQL database with name mentioned in `.env`:
    - Connect to MySQL instance `$ mysql -u root -p`.
    - Execute `mysql> CREATE DATABASE wunder_fleet;`
- Update `DATABASE_URL` in `.env` file to suite your local environment
- Installing Encore for Symfony Applications by running the following commmands inside project directory:
    - Run `$ composer require symfony/webpack-encore-bundle`
    - Run `$ yarn install`
    - Run `$ yarn add @symfony/webpack-encore --dev`
    - Run `$ yarn encore dev`
- Setup Database:
    - Run `$ php bin/console make:migration` to create migration
    - Run `$ php bin/console doctrine:migrations:migrate` to create entity tables
- Run `$ php bin/console server:run 127.0.0.1:9000` to view in local browser
    - Browse to `http://127.0.0.1:9000` and ensure you receive the home page.
